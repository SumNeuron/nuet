const wait = async (ms) => new Promise(resolve => { setTimeout(resolve, ms); });


export const asyncPlus = async (amount, by, delay) => {
  await wait(delay)
  return amount + by  
}
