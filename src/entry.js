import Vue from 'vue';


// import all your components
import AsyncSlot from './components/AsyncSlot.vue'
import AsyncCount from './components/AsyncCount.vue'
const components = {
  AsyncSlot,
  AsyncCount
}

// import all you VUEX modules
import module from './store/modules/nuet/index.js'



function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}


export { module }
// export { ...components }
export {
  AsyncSlot,
  AsyncCount
}
export const strict = false
export default components
