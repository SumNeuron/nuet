If you run `vue create <project>` and include `vuex` the store will be placed
at `src/store.js`.

Here we restructure:

```
src/store.js --> src/store/index.js
```

and any modules we want to make are under

```
src/store/modules/<module-name>/
    actions.js
    getters.js
    mutations.js
    state.js
    index.js
```
