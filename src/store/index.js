import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import module from './modules/nuet/index.js'

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    nuet: module
  }
})
