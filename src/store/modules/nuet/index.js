
import {asyncPlus} from '../../../utils/index.js'


const state = () => ({
   count: 0,
   delay: 1000,
   ready: true,
   msg: ''
})

const actions = {
  async plus({state, commit}) {
    if (state.ready) {
      if (state.msg) {
        commit('set', {k: 'msg', v: ''})
      }
      commit('set', {k: 'ready', v: false})
      let incremented = await asyncPlus(state.count, 1, state.delay)

      commit('set', {k: 'count', v: incremented})
      commit('set', {k: 'ready', v: true})
      if (state.msg) {
        commit('set', {k: 'msg', v: ''})
      }
    }
    else {
      commit('set', {k: 'msg', v: 'please wait for count to update'})
    }

  },

}

const mutations = {
  set(state, {k, v}) { state[k] = v },
}

const getters = {

}




export default {
  namespaced: true,
  state, actions, mutations, getters
}
