# nuet

This project serves as a template to solve a common problem.

You want to write a Vuex module, as well as some Vue components that interface
with it, that you can use in several projects. You want this module to work in
Nuxt via its "module" interface. In addition you want to write using arrow
functions `()=>{}` and `async / await`, but not lose backward compatibility.
You also want to sprinkle in / work with Vuetify components.


This project:

- exports two components (see `src/components`) that interface with the module
  + one of which includes a Vuetify component
- exports one exported vuex module (see `src/store/modules`)
- uses some non exported utility functions (see `src/utils`)


The exporting takes place in **`src/entry.js`**.

This [CodeSandbox](https://codesandbox.io/s/kw4o78ol1r?fontsize=14) shows it in action




## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Rollup
```
npm run r:build
```


### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
